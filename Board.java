class Board{
    char[][] data = new char[3][3];
    public  char[][] createTable() {
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data.length; col++) {
                data[row][col] = '-';
            }
        }
        return data;
    }
    public static void showTable(char[][] table){
        System.out.println("  1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.println((row + 1) + " " + table[row][0] + " " + table[row][1] + " " + table[row][2]);
        }

    }
}
import java.util.Scanner;

public class OX { 

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static char[][] createTable(char[][] table) {
        for (int row = 0; row < table.length; row++) {
            for (int col = 0; col < table.length; col++) {
                table[row][col] = '-';
            }
        }
        return table;
    }

    public static void showTable(char[][] table) {
        System.out.println("  1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.println((row + 1) + " " + table[row][0] + " " + table[row][1] + " " + table[row][2]);
        }

    }

    public static int input(char[][] table, char turn) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Turn " + turn);
        System.out.println("Please input Row Col : ");
        for (;;) {
            int Row = kb.nextInt();
            int Col = kb.nextInt();
            if (Row < 1 | Row > 3 | Col < 1 | Col > 3) {
                System.out.println("Please input Row Col again : ");
            } else if (table[Row - 1][Col - 1] == '-' & table[Row - 1][Col - 1] != turn
                    & table[Row - 1][Col - 1] != turn) {
                table[Row - 1][Col - 1] = turn;
                break;
            } else {
                System.out.println("Please input Row Col again : ");
            }
        }
        return 0;
    }

    public static char changeTurn(char turn) {
        if (turn == 'O') {
            turn = 'X';
        } else {
            turn = 'O';
        }
        return turn;
    }

    public static boolean checkFlat(char[][] table, char turn) {

        for (int row = 0; row < table.length; row++) {
            if (table[row][0] == turn & table[row][1] == turn & table[row][2] == turn) {

                return true;
            }
        }
        return false;
    }

    public static boolean checkVertical(char[][] table, char turn) {
        for (int col = 0; col < table.length; col++) {
            if (table[0][col] == turn & table[1][col] == turn & table[2][col] == turn) {

                return true;
            }
        }
        return false;
    }

    public static boolean checkObliqueRight(char[][] table, char turn) {
        for (int row = 0; row < table.length; row++) {
            if (table[0][0] == turn & table[1][1] == turn & table[2][2] == turn) {

                return true;
            }
        }
        return false;
    }

    public static boolean checkObliqueLeft(char[][] table, char turn) {

        for (int row = 0; row < table.length; row++) {
            if (table[0][2] == turn & table[1][1] == turn & table[2][0] == turn) {

                return true;
            }
        }
        return false;
    }

    public static boolean checkDraw(char[][] table) {
        int countDraw = 0;
        for (int row = 0; row < table.length; row++) {
            for (int col = 0; col < table.length; col++) {
                if (table[row][col] != '-') {
                    countDraw++;
                }
                if (countDraw == 8) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean checkWin(char[][] table, char turn) {
        return checkObliqueRight(table, turn) || checkObliqueLeft(table, turn) || checkVertical(table, turn)
                || checkFlat(table, turn);
    }

    public static void showWin(char turn) {
        System.out.println("Player " + turn + " win");
        System.out.println("Bye bye");
    }

    public static void showDraw() {
        System.out.println("Draw");
        System.out.println("Bye bye");

    }

    public static void main(String[] args) {
        char turn = 'O';
        char[][] table = new char[3][3];

        showWelcome();
        table = createTable(table);
        for (;;) {
            showTable(table);
            input(table, turn);

            if (checkWin(table, turn)) {
                showTable(table);
                showWin(turn);
                break;
            } else if (checkDraw(table)) {
                showTable(table);
                showDraw();
                break;
            }
            turn = changeTurn(turn);

        }
    }

}
